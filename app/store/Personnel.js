Ext.define('Pertemuan.store.Personnel', {
    extend: 'Ext.data.Store',
    storeId: 'personnel',

    alias: 'store.personnel',

    fields: [
        'photo', 'npm', 'name', 'email', 'phone', 'kategori'
    ],

    data: { items: [
        { kategori: 'IT', photo: 'resources/020.jpg', npm: '183510492', name: 'Agro Tambas', email: "agro1tambas@gmail.com", phone: "555-111-1111" },
        { kategori: 'IT', photo: 'resources/020.jpg', npm: '183510450', name: 'Agra',       email: "agra@gmail.com", phone: "555-111-1111" },
        { kategori: 'Psi', photo: 'resources/017.jpg', npm: '203510001', name: 'Worf',     email: "worf.moghsson@enterprise.com",  phone: "555-222-2222" },
        { kategori: 'Psi', photo: 'resources/018.jpg', npm: '193510002', name: 'Deanna',   email: "deanna.troi@enterprise.com",    phone: "555-333-3333" },
        { kategori: 'IT', photo: 'resources/019.jpg', npm: '213510003', name: 'Data',     email: "mr.data@enterprise.com",        phone: "555-444-4444" }
    ]},

    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    }
});
