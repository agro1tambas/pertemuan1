Ext.define('Pertemuan.view.tree.TreePanel', {
    extend: 'Ext.Container',
    xtype: 'tree-panel',

    requires: [
        'Ext.layout.HBox',
        'Pertemuan.view.tree.Tree'
    ],

    layout: {
        type: 'hbox',
        pack: 'center',
        align: 'stretch'
    },
    margin: '0 10',
    defaults: {
        margin: '0 0 10 0',
        bodyPadding: 10
    },
    items: [
        {
            xtype: 'tree-list',
            flex: 1
        },
        {
            xtype: 'mainlist',
            flex: 1
        }
    ]
});