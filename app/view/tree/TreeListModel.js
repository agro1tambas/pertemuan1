Ext.define('Pertemuan.view.tree.TreeListModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.tree-list',

    formulas: {
        selectionText: function(get) {
            var selection = get('treelist.selection'),
                path;
            if (selection) {
                path = selection.getPath('text');
                path = path.replace(/^\/Root/, '');
                return 'Selected: ' + path;
            } else {
                return 'No node selected';
            }
        }
    }, 

    stores: {
        navItems: {
            type: 'tree',
            rootVisible: true,
            root: {
                expanded: true,
                text: 'Kategori',
                iconCls: 'x-fa fa-sitemap',
                children: [{
                    text: 'IT',
                    iconCls: 'x-fa fa-home',
                    leaf: true,
                    id: 'it',
                }, {
                    text: 'Psi',
                    iconCls: 'x-fa fa-user',
                    leaf: true,
                    id: 'psi',
                }]
            }
        }
    }
});