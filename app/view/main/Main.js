/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting causes an instance of this class to be created and
 * added to the Viewport container.
 *
 * TODO - Replace the content of this view to suit the needs of your application.
 */
Ext.define('Pertemuan.view.main.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'app-main',
    id: 'app-main',

    requires: [
        'Ext.MessageBox',

        'Pertemuan.view.main.MainController',
        'Pertemuan.view.main.MainModel',
        'Pertemuan.view.main.List',
        'Pertemuan.view.form.User',
        'Pertemuan.view.group.Carousel',
        'Pertemuan.view.setting.BasicDataView',
        'Pertemuan.view.chart.Column',
        'Pertemuan.view.form.Login',
        'Pertemuan.view.chart2.Bar',
        'Pertemuan.view.chart3.Pie',
        'Pertemuan.view.tree.TreePanel',
    ],

    controller: 'main',
    viewModel: 'main',

    defaults: {
        tab: {
            iconAlign: 'top'
        },
        //styleHtmlContent: true
    },

    tabBarPosition: 'bottom',

    items: [
        {
            title: 'Home',
            iconCls: 'x-fa fa-home',
            layout: 'fit',
            // The following grid shares a store with the classic version's grid as well!
            items: [{
                xtype: 'mainlist'
            }]
        },{
            title: 'Users',
            iconCls: 'x-fa fa-user',
            layout: 'fit',
            items: [{
                xtype: 'user'
            }]
        },{
            title: 'Groups',
            iconCls: 'x-fa fa-users',
            layout: 'fit',
            items: [{
                xtype: 'mycarousel'
            }]
        },{
            title: 'Settings',
            iconCls: 'x-fa fa-cog',
            layout: 'fit',
            items: [{
                xtype: 'basicdataview'
            }]
        },{
            title: 'Chart',
            iconCls: 'x-fa fa-cog',
            layout: 'fit',
            items: [{
                xtype: 'column-chart'
            }]
        },{
            title: 'Chart 2',
            iconCls: 'x-fa fa-cog',
            layout: 'fit',
            items: [{
                xtype: 'bar-chart'
            }]
        },{
            title: 'Chart 3',
            iconCls: 'x-fa fa-user',
            layout: 'fit',
            items: [{
                xtype: 'chart'
            }]
        },{
            title: 'Tree',
            iconCls: 'x-fa fa-user',
            layout: 'fit',
            items: [{
                xtype: 'tree-panel'
            }]
        }
    ]
});
