/**
 * This view is an example list of people.
 */
Ext.define('Pertemuan.view.main.List', {
    extend: 'Ext.grid.Grid',
    xtype: 'mainlist',

    requires: [
        'Pertemuan.store.Personnel'
    ],

    title: 'Data Anggota Agro Tambas',

    store: {
        type: 'personnel'
    },

    columns: [
        { text: 'Kategori',  dataIndex: 'kategori', width: 60 },
        { text: 'Npm',  dataIndex: 'npm', width: 130 },
        { text: 'Nama',  dataIndex: 'name', width: 100 },
        { text: 'Email', dataIndex: 'email', width: 210 },
        { text: 'Telepon', dataIndex: 'phone', width: 150 }
    ],

    listeners: {
        select: 'onItemSelected'
    }
});
