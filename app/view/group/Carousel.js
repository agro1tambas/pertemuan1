Ext.define('Pertemuan.view.group.Carousel', {
    extend: 'Ext.Container',
    xtype: 'mycarousel',

    requires: [
        'Ext.carousel.Carousel'
    ],

    shadow: true,
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    defaults: {
        flex: 1
    },
    items: [{
        xtype: 'carousel',
        direction: 'vertical',
        items: [{
            html: '<p>Carousels can also be vertical <em>(swipe up)…</p>',
        },
        {
            html: 'And can also use <code>ui:light</code>.',
        },
        {
            html: 'Card #3',
        }]
    }, {
        xtype: 'spacer'
    }, {
        xtype: 'carousel',
        items: [{
            html: '<p>Carousels can also be vertical <em>(swipe up)…</p>',
        },
        {
            html: 'And can also use <code>ui:light</code>.',
        },
        {
            html: 'Card #3',
        }]
    },{
        xtype: 'spacer'
    }, {
        xtype: 'carousel',
        direction: 'vertical',
        items: [{
            html: '<p>Carousels can also be vertical <em>(swipe up)…</p>',
        },
        {
            html: 'And can also use <code>ui:light</code>.',
        },
        {
            html: 'Card #3',
        }]
    }]
});